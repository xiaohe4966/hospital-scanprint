package cn.he4966.sacnprint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;



import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.company.TxScanner;
//导入本地包

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.ArrayList;

import cn.hutool.http.HttpUtil;
@SpringBootApplication
public class SacnprintApplication {
    static TxScanner cTxScanner = new TxScanner();
    static int s_uiDecodeCnt = 0;
    static String usUartName;
    static int uiBaudrate;

    public static void main(String[] args) throws InterruptedException, IOException {
        int iRet = -1;
        int iParam;
        int iSaveParam;

        TxScanner.TsDecodeDataCbFun fTsDecodeData = new TxScanner.TsDecodeDataCbFun() {
            @Override
            public int tx_sanner_decode_data_cb_fun(byte ucCodeType, byte[] pBuf, int uiBufLen) {
                String strDecodeData = new String(pBuf);

//                System.out.printf("[%03d][%02d]len:%d,data:",++s_uiDecodeCnt, ucCodeType, uiBufLen);
//                for (byte b : pBuf) {
//                    System.out.printf("%c", b);
//                }
//                System.out.println();
                System.out.println(strDecodeData);

                //开始调用http接口
                HashMap<String, Object> paramMap = new HashMap<>();
                paramMap.put("str", strDecodeData);

                String result= HttpUtil.get("https://m24.skita.cn/api/yuyue/com/scan_qrcode", paramMap);
                System.out.printf( result);
                try {
                    JSONObject json = JSONUtil.parseObj(result);
                    if(json.getInt("code")>0){
                        json.getStr("name");
                        JSONObject data =  json.getJSONObject("data");

                        String order_sn = data.getStr("order_sn");
                    }else{

                    }
                } catch (Exception e) {
                    System.out.println("访问出错"+e.getMessage());
                }




//				System.out.printf("[%03d][%02d]len:%d,data:%s\r\n",++s_uiDecodeCnt, ucCodeType, uiBufLen, strDecodeData);
                return 0;
            }
        };

        TxScanner.TsCommStateCbFun fTsCommState = new TxScanner.TsCommStateCbFun() {
            @Override
            public int tx_sanner_comm_state_cb_fun(byte ucState) {
                /**0:断开连接,1:连接**/
                System.out.printf("scanner comm state:%d\r\n", ucState);
                return 0;
            }
        };

        /**打印版本信息**/
        System.out.printf("java uart demo ver:%s [%d]\r\n", cTxScanner.tx_scanner_get_version(), cTxScanner.tx_scanner_get_product_type());

        /**注册解码数据回调函数**/
        cTxScanner.tx_scanner_get_data_fun_register(fTsDecodeData);
        /**注册设备连接状态回调函数**/
        cTxScanner.tx_scanner_state_fun_register(fTsCommState);

        /**初始化**/
        cTxScanner.tx_scanner_init();

        /**获取系统可用串口号**/
        ArrayList<String> portNameList = cTxScanner.tx_scanner_uart_findPorts();
        System.out.println("findPorts:");
        for (String portName : portNameList) {
            System.out.println(portName);
        }



        /**打开串口**/
        Scanner sc = new Scanner(System.in);
//		System.out.println("input ports:");
//		 usUartName = sc.next();
//		System.out.println("input baud:");
//		uiBaudrate = sc.nextInt();

        System.out.printf("portNameList.size", portNameList.size());
        if(portNameList.size()==1){
            usUartName = portNameList.get(0);
        }else{

            //  如果数组里面有COM3
            if(portNameList.contains("COM3")){
                usUartName = "COM3";
            }else{
                System.out.println("input ports:");
                usUartName = sc.next();
            }

        }
        uiBaudrate = 115200;

        iRet = cTxScanner.tx_scanner_uart_open(usUartName, uiBaudrate);
        if(iRet < 0)
            System.out.println("ts_scan_uart_open FAIL!!!");
        else {
            System.out.println("open success");
            show_directory();
        }

        while(true)
        {
//            switch(System.in.read())
            switch(sc.next().charAt(0))
            {
                case 'a':
                case 'A':
                    /**开始扫描**/
                    iRet = cTxScanner.tx_scanner_decode_start();
                    if(-1 == iRet)
                        System.out.print("tx_scanner_decode_start FAIL!!!\r\n");
                    else
                        System.out.print("start\r\n");
                    break;

                case 'b':
                case 'B':
                    /**结束扫描**/
                    iRet = cTxScanner.tx_scanner_decode_stop();
                    if(-1 == iRet)
                        System.out.print("tx_scanner_decode_stop FAIL!!!\r\n");
                    else
                        System.out.print("stop\r\n");
                    break;

                case 'c':
                case 'C':
                    /**开始不超时解码**/
                    iRet = cTxScanner.tx_scanner_no_time_decode_start();
                    if(iRet < 0)
                        System.out.printf("tx_scanner_no_time_decode_start fail!!!\r\n");
                    else
                        System.out.printf("no time start\r\n");
                    break;

                case 'd':
                case 'D':
                    /**获取版本信息**/
                    byte[] ucVerInfo = new byte[128];
                    iRet = cTxScanner.tx_scanner_get_version_info(ucVerInfo, ucVerInfo.length);
                    if(iRet < 0)
                    {
                        System.out.printf("tx_scanner_get_version_info fail!!!\r\n");
                    }
                    else
                    {
                        String strVerInfo = new String(ucVerInfo);
                        System.out.printf("%s\r\n", strVerInfo);
                    }
                    break;

                case 'e':
                case 'E':
                    /**获取产品信息**/
                    byte[] ucProInfo = new byte[256];
                    iRet = cTxScanner.tx_scanner_get_all_product_info(ucProInfo, ucProInfo.length);
                    if(iRet < 0)
                    {
                        System.out.printf("tx_scanner_get_all_product_info fail!!!\r\n");
                    }
                    else
                    {
                        String strProInfo = new String(ucProInfo);
                        System.out.printf("%s\r\n", strProInfo);
                    }
                    break;

                case 'f':
                case 'F':
                    /**设置触发方式**/
                    System.out.printf("trigger mode,0:Key hold,1:A single press,2:Continuous mode,3:Automatic induction mode\r\n");
                    iParam = sc.nextInt();

                    System.out.printf("0:Temporary parameters,1:Permanent parameters\r\n");
                    iSaveParam = sc.nextInt();

                    System.out.printf("Input parameters:%d,%d\r\n", iParam, iSaveParam);
                    iRet = cTxScanner.tx_scanner_set_trigger_mode((byte)iParam, (byte)iSaveParam);
                    if(iRet < 0)
                        System.out.printf("tx_scanner_set_trigger_mode fail!!!\r\n");
                    break;

                case 'g':
                case 'G':
                    /**获取触发方式**/
                    iRet = cTxScanner.tx_scanner_get_trigger_mode();
                    System.out.printf("tx_scanner_get_trigger_mode:%d\r\n", iRet);
                    break;

                case 'h':
                case 'H':
                    /**补光灯控制**/
                    System.out.printf("light LED,0:working light,1:always off,2:always on\r\n");
                    iParam = sc.nextInt();

                    System.out.printf("0:Temporary parameters,1:Permanent parameters\r\n");
                    iSaveParam = sc.nextInt();

                    System.out.printf("Input parameters:%d,%d\r\n", iParam, iSaveParam);
                    iRet = cTxScanner.tx_scanner_set_light_state((byte)iParam, (byte)iSaveParam);
                    if(iRet < 0)
                        System.out.printf("tx_scanner_set_light_state fail!!!\r\n");
                    break;

                case 'i':
                case 'I':
                    /**获取补光灯状态**/
                    iRet = cTxScanner.tx_scanner_get_light_state();
                    System.out.printf("tx_scanner_get_light_state:%d\r\n", iRet);
                    break;

                case 'j':
                case 'J':
                    /**定位灯控制**/
                    System.out.printf("focus LED,0:working light,1:always off,2:always on\r\n");
                    iParam = sc.nextInt();

                    System.out.printf("0:Temporary parameters,1:Permanent parameters\r\n");
                    iSaveParam = sc.nextInt();

                    System.out.printf("Input parameters:%d,%d\r\n", iParam, iSaveParam);
                    iRet = cTxScanner.tx_scanner_set_focus_state((byte)iParam, (byte)iSaveParam);
                    if(iRet < 0)
                        System.out.printf("tx_scanner_set_focus_state fail!!!\r\n");
                    break;

                case 'k':
                case 'K':
                    /**获取定位灯状态**/
                    iRet = cTxScanner.tx_scanner_get_focus_state();
                    System.out.printf("tx_scanner_get_focus_state:%d\r\n", iRet);
                    break;

                case 'l':
                case 'L':
                    /**重新初始化**/
                    iRet = cTxScanner.tx_scanner_init();
                    if(iRet < 0) {
                        System.out.println("do not repeat init,init fail");
                    }else {
                        System.out.printf("scanner init success\r\n");
                        System.out.println("findPorts:");
                        for (String portName : portNameList) {
                            System.out.println(portName);
                        }
                        /**打开串口**/
                        System.out.println("input ports:");
                        usUartName = sc.next();
                        System.out.println("input baud:");
                        uiBaudrate = sc.nextInt();

                        iRet = cTxScanner.tx_scanner_uart_open(usUartName, uiBaudrate);
                        if (iRet < 0)
                            System.out.println("ts_scan_uart_open FAIL!!!");
                        else {
                            System.out.println("open success");
                            s_uiDecodeCnt = 0;
                            show_directory();
                        }
                    }
                    break;

                case 'm':
                case 'M':
                    /**去除初始化**/
                    cTxScanner.tx_scanner_deinit();
                    break;

                case '?':
                    show_directory();
                    break;

                case 'q':
                case 'Q':
                    System.exit(1);
                    break;

                default:
                    break;
            }
        }
    }

    static void show_directory(){
        System.out.printf("******************************************************\r\n");
        System.out.printf("a:start decode\r\n");
        System.out.printf("b:stop decode\r\n");
        System.out.printf("c:no time decode start\r\n");
        System.out.printf("d:get version info\r\n");
        System.out.printf("e:get product info\r\n");
        System.out.printf("f:set trigger mode\r\n");
        System.out.printf("g:get trigger mode\r\n");
        System.out.printf("h:set light LED state\r\n");
        System.out.printf("i:get light LED state\r\n");
        System.out.printf("j:set focus LED state\r\n");
        System.out.printf("k:get fucus LED state\r\n");
        System.out.printf("l:scanner init\r\n");
        System.out.printf("m:scanner deinit\r\n");
        System.out.printf("?:get Menu\r\n");
        System.out.printf("q:quit\r\n");
        System.out.printf("******************************************************\r\n");
    }
}

